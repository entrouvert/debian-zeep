appdirs>=1.4.0
cached-property>=1.3.0
defusedxml>=0.4.1
isodate>=0.5.4
lxml>=3.0.0
requests>=2.7.0
requests-toolbelt>=0.7.1
six>=1.9.0
pytz

[async]
aiohttp>=1.0

[docs]
sphinx>=1.4.0

[test]
freezegun==0.3.8
mock==2.0.0
pretend==1.0.8
pytest-cov==2.5.1
pytest==3.1.3
requests_mock>=0.7.0
pytest-tornado==0.4.5
isort==4.2.15
flake8==3.3.0
flake8-blind-except==0.1.1
flake8-debugger==1.4.0
flake8-imports==0.1.1
aioresponses>=0.1.3

[tornado]
tornado>=4.0.2

[xmlsec]
xmlsec>=0.6.1
